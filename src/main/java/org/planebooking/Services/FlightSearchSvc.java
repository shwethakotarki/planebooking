package org.planebooking.Services;

import org.planebooking.Repository.FlightRepository;
import org.planebooking.model.FlightRide;
import org.planebooking.model.RideFare;
import org.planebooking.model.SearchCriteria;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
public class FlightSearchSvc {
    private FlightRepository flightRep = new FlightRepository();

    public FlightSearchSvc() {

    }

    public FlightSearchSvc(FlightRepository rep) {
        this.flightRep = rep;
    }


    public HashMap<FlightRide,RideFare> searchFlights(SearchCriteria search) {


        ArrayList searchList = new ArrayList();
        ArrayList<FlightRide> flightList = flightRep.initData();

        if (search.getNoOfPass() == null)
            search.setNoOfPass(1);

        if (search.getDateOfTravel() == null)
            search.setdateOfTravel(java.time.LocalDate.now().plusDays(1));

        if (search.getClassType() == null || search.getClassType().equals("0"))
            search.setClassType("EC");


        //filter 1
        Predicate<FlightRide> isMatchingSource = flightRide -> flightRide.getSource().equalsIgnoreCase(search.getOrigin());

        //filter2
        Predicate<FlightRide> isMatchingWithDest = flightRide -> flightRide.getSource().equalsIgnoreCase(search.getOrigin());

        //filter3
        Predicate<FlightRide> isMatchingNoOfPass = flightRide -> Integer.parseInt(flightRide.getAvailableSeats()) >= search.getNoOfPass();

        //filter3
        Predicate<FlightRide> isMatchingWithDate = flightRide -> (flightRide.getflightPlannedStartDate().equals(search.getDateOfTravel()));

        //filter4
        // Predicate<FlightRide> isMatchingWithClass = flightRide -> flightRide.getFlightInfo().getFlightClass().containsKey(search.getClassType());

        List<FlightRide> result = flightList.stream().filter(isMatchingSource.and(isMatchingWithDest)
                .and(isMatchingNoOfPass).and(isMatchingWithDate)).collect(Collectors.toList());

        HashMap<FlightRide,RideFare> filteredFlightsMapByClass = new HashMap<FlightRide,RideFare>();

        String classTypeString = "";
        Integer classTypeVal;
        ArrayList<RideFare> rideFareList = flightRep.initRideFareData();

        for (FlightRide flightRide : result) {

            HashMap<String, Integer> classType = new HashMap<String, Integer>();
            classType = flightRide.getFlightInfo().getFlightClass();

            if (classType.containsKey(search.getClassType()) && classType.get(search.getClassType()) > 0) {
                classTypeString = search.getClassType();
                String finalClassTypeString = classTypeString;
                Integer rideID = flightRide.getRideID();

                Predicate<RideFare> matchClassType = rideFare1 -> rideFare1.getClassType().equalsIgnoreCase(finalClassTypeString);

                Predicate<RideFare> matchableRideID = rideFare1 -> rideFare1.getRideID().equals(rideID);

                RideFare rideFare = rideFareList.stream().filter(matchClassType.and(matchableRideID)).findAny().orElse(null);
                //..findAny().get();


                switch (classTypeString) {

                    case "EC":
                        if(calculateECFare(search, rideFare, classType) > 0.0) {
                            //rideFare.setTotalCost(calculateECFare(search, rideFare, classType));
                            filteredFlightsMapByClass.put(flightRide,rideFare);
                        }
                        break;
                    case "BC":

                        if(calculateBCFare(search, flightRide, rideFare) > 0.0) {
                            //rideFare.setTotalCost(calculateBCFare(search, flightRide, rideFare));
                            filteredFlightsMapByClass.put(flightRide,rideFare);
                        }
                        break;
                    case "FC":
                        if(calculateFCFare(search, rideFare) > 0.0) {
                            //rideFare.setTotalCost(calculateFCFare(search, rideFare));
                            filteredFlightsMapByClass.put(flightRide,rideFare);
                        }
                        break;
                }
            }

        }
        return filteredFlightsMapByClass;
    }

    private double calculateECFare(SearchCriteria search, RideFare rideFare, HashMap<String, Integer> classType) {
        double totalCost = 0;
        int temp = 0;
        double basefare = rideFare.getFare();
        Integer ECavailableSeats = rideFare.getAvailableSeatsinClass();
        //Integer totalFlightCapacity = classType.values().stream().reduce(0, Integer::sum);
        Integer classCapacity = classType.get(search.getClassType());

        Integer bookedTicketCount = classCapacity - ECavailableSeats;
                                                                                //if(100 is the seatcapacity,  )
        Integer first40PercentSeats = (int) (classCapacity * (40.0f / 100.0f)); //first 40% = 1-40 seats
        Integer next50PercentSeats = (int) (classCapacity * (50.0f / 100.0f));//next 50% = 41-91 seats
        Integer last10PercentSeats = (int) (classCapacity * (10.0f / 100.0f));//next 10 % = 91-100 seats


        while (!search.getNoOfPass().equals(temp)){//loops through the no. of passengers entered

            if (bookedTicketCount + 1 <= first40PercentSeats) { //need to start checking from the next number of booked ticket
                totalCost += basefare;
                bookedTicketCount++;
                temp++;
            } else if (bookedTicketCount + 1 >= first40PercentSeats + 1 && bookedTicketCount + 1 <= (first40PercentSeats + next50PercentSeats)) {
                double next50PercentFare = (double) (basefare * (30.0f / 100.0f)) + basefare;
                totalCost += next50PercentFare;
                bookedTicketCount++;
                temp++;
            } else if (bookedTicketCount + 1 >= (first40PercentSeats + next50PercentSeats) + 1 && bookedTicketCount + 1 <= classCapacity) {
                double last10PercentFare = (double) (basefare * (60.0f / 100.0f)) + basefare;
                totalCost += last10PercentFare;
                bookedTicketCount++;
                temp++;
            }
        }
        rideFare.setTotalCost(totalCost);
        return Math.round(totalCost);
    }

    private double calculateBCFare(SearchCriteria search, FlightRide flightRide, RideFare rideFare) {

        double totalFare = 0.0;
        Integer noOfPass = search.getNoOfPass();
        DayOfWeek dayOfWeek = search.getDateOfTravel().getDayOfWeek();
        double basefare = rideFare.getFare();
        Period period = Period.between(LocalDate.now(),search.getDateOfTravel());
        Integer diff = period.getDays();

        if( diff <= 28 ) {

            if (dayOfWeek == DayOfWeek.MONDAY || dayOfWeek == DayOfWeek.FRIDAY || dayOfWeek == DayOfWeek.SUNDAY) {
                double fourtyPercentofBaseFare = (double) (basefare * (40.0f / 100.0f)) + basefare;
                totalFare = noOfPass * fourtyPercentofBaseFare;

            } else {
                totalFare = noOfPass * basefare;
            }
            rideFare.setTotalCost(totalFare);

        }
        return Math.round(totalFare);
    }

    private double calculateFCFare(SearchCriteria search, RideFare rideFare) {

        double extraFare = 0.0;
        double totalFare = 0.0;
        Integer noOfPass = search.getNoOfPass();
        double basefare = rideFare.getFare();
        LocalDate currDate = LocalDate.now();
        Period period = Period.between(currDate,search.getDateOfTravel());
        Integer diff = period.getDays();

        if(diff <= 10) {
            switch (diff) {

                case 1:
                    extraFare = (double) (basefare * (100.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 2:
                    extraFare = (double) (basefare * (90.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 3:
                    extraFare = (double) (basefare * (80.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 4:
                    extraFare = (double) (basefare * (70.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 5:
                    extraFare = (double) (basefare * (60.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 6:
                    extraFare = (double) (basefare * (50.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 7:
                    extraFare = (double) (basefare * (40.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 8:
                    extraFare = (double) (basefare * (30.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 9:
                    extraFare = (double) (basefare * (20.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;
                case 10:
                    extraFare = (double) (basefare * (10.0f / 100.0f)) + basefare;
                    totalFare = extraFare * noOfPass;
                    break;

            }
        }
        rideFare.setTotalCost(totalFare);
        return totalFare;
    }

    public ArrayList getFlightsInList(SearchCriteria search)
    {
        ArrayList lst = new ArrayList();
        HashMap<FlightRide, RideFare> flights = searchFlights(search);
        for (Map.Entry<FlightRide,RideFare> entry : flights.entrySet())
           lst.add ( "Flight Carrier:" + entry.getKey().getFlightInfo().getCarrier() + "<br>"
                   +"Source:" + entry.getKey().getSource() + "<br>"
                    + "Destination:" + entry.getKey().getDestination() + "<br>"
                   // + "Flight Model:" + entry.getKey().getFlightInfo().getFlightModel() + "<br>"

                    //+ "Flight Ride ID:" + entry.getValue().getRideID() + "<br>"
                    + "Flight Class Type:" + entry.getValue().getClassType() + "<br>"
                           + "No Of Passengers:" + search.getNoOfPass()+ "<br>"
                    + "Flight Base Fare:" + entry.getValue().getFare() + "<br>"
                    + "Total Cost:" + (int)entry.getValue().getTotalCost());
        return lst;
    }
}




