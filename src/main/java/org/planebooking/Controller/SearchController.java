package org.planebooking.Controller;

import org.planebooking.Repository.FlightRepository;
import org.planebooking.Services.FlightSearchSvc;
import org.planebooking.model.FlightRide;
import org.planebooking.model.RideFare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import org.planebooking.Services.FlightSearchSvc;
import org.planebooking.model.AjaxResponseBody;
import org.planebooking.model.SearchCriteria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class SearchController {

    @Autowired
    FlightSearchSvc fltSearchSvc;


    @PostMapping("/api/search")
    public ResponseEntity<?> getSearchResultViaAjax(
            @Valid @RequestBody SearchCriteria search, Errors errors) {

        AjaxResponseBody result = new AjaxResponseBody();
       // AjaxResponseBody result2 = new AjaxResponseBody();

        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            return ResponseEntity.badRequest().body(result);

        }

        //List<FlightRide> flights= fltSearchSvc.searchFlights(search.getOrigin(),search.getDest(),search.getNoOfPass(),search.getDateOfTravel());

        //List<FlightRide> flights= fltSearchSvc.searchFlights(search);
       // HashMap<FlightRide, RideFare> flights = fltSearchSvc.searchFlights(search);
        ArrayList listOfFlights = fltSearchSvc.getFlightsInList(search);


       //   List<FlightRide> flights= fltSearchSvc.searchFlights(search.getOrigin(),search.getDest(),Integer.parseInt(search.getNoOfPass()),search.getDateOfTravel());

        if (listOfFlights.isEmpty()) {
            result.setMsg("no flights found!");
        } else {
            result.setMsg("success");
        }

        /*for (Map.Entry<FlightRide,RideFare> entry : flights.entrySet())
            result.setMsg("Source:" + entry.getKey().getSource() + "<br>"
                    + "Destination:" + entry.getKey().getDestination() + "<br>"
                    + "Flight Model:" + entry.getKey().getFlightInfo().getFlightModel() + "<br>"
                    + "Flight Carrier:" + entry.getKey().getFlightInfo().getCarrier() + "<br>"
                    + "Flight Ride ID:" + entry.getValue().getRideID() + "<br>"
                    + "Flight Class Type:" + entry.getValue().getClassType() + "<br>"
                    + "Flight Base Fare:" + entry.getValue().getFare() + "<br>"
                    + "Total Cost:" + entry.getValue().getTotalCost() + "<br>"
                    + "No Of Passengers:" + search.getNoOfPass()); */



       // result.setMsg(flights.keySet().toString());

       result.setResult(listOfFlights);

       // result.setRideFare();


        return ResponseEntity.ok(result);


    }
}
