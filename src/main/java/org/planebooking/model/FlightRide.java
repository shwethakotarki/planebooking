package org.planebooking.model;
import org.apache.tomcat.jni.Local;

import java.time.LocalDate;

public class FlightRide {
    Integer rideID;
    Integer flightID;
    String source;
    String destination;
    String availableSeats;
    LocalDate flightPlannedStartDate ;
    FlightInfo flightInfo;

    public FlightRide(Integer rideID,Integer flightID, String source, String destination, String availableSeats, LocalDate flightPlannedStartDate, FlightInfo flightInfo) {
        this.rideID = rideID;
        this.flightID = flightID;
        this.source = source;
        this.destination = destination;
        this.availableSeats = availableSeats;
        this.flightPlannedStartDate = flightPlannedStartDate;
        this.flightInfo = flightInfo;
        //this.departureDate = departureDate;
    }


    public Integer getRideID() {
        return rideID;
    }

    public void setRideID(Integer rideID) {
        this.rideID = rideID;
    }

    public LocalDate getflightPlannedStartDate() {
        return flightPlannedStartDate;
    }

    public void setFlightPlannedStartDate(LocalDate flightPlannedStartDate) {
        this.flightPlannedStartDate = flightPlannedStartDate;
    }

    public FlightInfo getFlightInfo() {
        return flightInfo;
    }

    public void setFlightInfo(FlightInfo flightInfo) {
        this.flightInfo = flightInfo;
    }

    public Integer getFlightID() {
        return flightID;
    }

    public void setFlightID(Integer flightID) {
        this.flightID = flightID;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(String availableSeats) {
        this.availableSeats = availableSeats;
    }

}
