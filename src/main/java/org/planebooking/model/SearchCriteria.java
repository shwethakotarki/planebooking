package org.planebooking.model;

//import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

public class SearchCriteria {

    @NotBlank(message = "Origin can't be empty!")
    String origin;
    @NotBlank(message = "Destination can't be empty!")
    String dest;
    Integer noOfPass;
    LocalDate dateOfTravel;
    String classType;
    //double totalCost;

    public void setdateOfTravel(LocalDate dateOfTravel) {
        this.dateOfTravel = dateOfTravel;
    }

    public LocalDate getDateOfTravel() {
        return dateOfTravel;
    }

    public Integer getNoOfPass() {
        return noOfPass;
    }

    public void setNoOfPass(Integer noOfPass) {
        this.noOfPass = noOfPass;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getOrigin() {
        return origin;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }


}
