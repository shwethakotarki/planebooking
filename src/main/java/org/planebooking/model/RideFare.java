package org.planebooking.model;

public class RideFare {

    Integer rideID;
    Integer fareID;
    Integer availableSeatsinClass;
    String classType;
    double fare;
    double totalCost;

    public RideFare(Integer rideID, Integer fareID, String classType, Integer availableSeatsinClass, double fare) {

        this.rideID = rideID;
        this.fareID = fareID;
        this.classType = classType;
        this.fare = fare;
        this.availableSeatsinClass = availableSeatsinClass;
    }

    public Integer getRideID() {
        return rideID;
    }

    public void setRideID(Integer rideID) {
        this.rideID = rideID;
    }

    public Integer getFareID() {
        return fareID;
    }

    public Integer getAvailableSeatsinClass() {
        return availableSeatsinClass;
    }

    public void setAvailableSeatsinClass(Integer availableSeatsinClass) {
        this.availableSeatsinClass = availableSeatsinClass;
    }

    public void setFareID(Integer fareID) {
        this.fareID = fareID;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
}

