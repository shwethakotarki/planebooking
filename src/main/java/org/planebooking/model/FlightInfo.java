package org.planebooking.model;

import java.util.HashMap;

public class FlightInfo {

    Integer flightID;
    String flightModel;
    String carrier;
    public HashMap<String,Integer> flightClass = new HashMap<String,Integer>();

    public FlightInfo(Integer flightID, String flightModel, String carrier, HashMap<String, Integer> flightClass) {

        this.flightID = flightID;
        this.flightModel = flightModel;
        this.carrier = carrier;
        this.flightClass = flightClass;
    }

    public Integer getFlightID() {
        return flightID;
    }

    public void setFlightID(Integer flightID) {
        this.flightID = flightID;
    }

    public String getFlightModel() {
        return flightModel;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public void setFlightModel(String flightModel) {
        this.flightModel = flightModel;
    }

    public HashMap<String, Integer> getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(HashMap<String, Integer> flightClass) {
        this.flightClass = flightClass;
    }
}
