package org.planebooking.Repository;
import org.planebooking.model.FlightInfo;
import org.planebooking.model.FlightRide;
import org.planebooking.model.RideFare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FlightRepository {

    public ArrayList<FlightRide> initData()
    {
      ArrayList listOfFlights = new ArrayList();
      HashMap<String,Integer> flightClass = new HashMap<String,Integer>();
      HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
      HashMap<String,Integer> flightClass2 = new HashMap<String,Integer>();
      HashMap<String,Integer> flightClass3 = new HashMap<String,Integer>();

      FlightInfo flightInfo;
      FlightRide flightRide;

      flightClass.put("EC",0);
      flightClass.put("BC",20);
      flightClass.put("FC",30);

      FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass);
      FlightRide flightRide1 = new FlightRide(100,1,"HYD","BLR","15",LocalDate.now(),flightInfo1);
      FlightRide flightRide2 = new FlightRide(101,1,"BLR","HYD","20",LocalDate.now(),flightInfo1);


      flightClass1.put("EC",50);
      flightClass1.put("BC",0);
      flightClass1.put("FC",10);
        //LocalDate.parse("2019-08-21")
      FlightInfo flightInfo2 = new FlightInfo(2,"Airbus A319 V2","SPICE JET",flightClass1);
      FlightRide flightRide3 = new FlightRide(102,2,"HYD","BLR","30",LocalDate.now().plusDays(4),flightInfo2);
      FlightRide flightRide4 = new FlightRide(103,2,"BLR","HYD","30",LocalDate.now(),flightInfo2);


        flightClass2.put("EC",50);
        flightClass2.put("BC",10);
        flightClass2.put("FC",10);
        //LocalDate.parse("2019-08-21")
        FlightInfo flightInfo3 = new FlightInfo(3,"Boeing 787","Indigo",flightClass2);
        FlightRide flightRide5 = new FlightRide(104,3,"HYD","BLR","30",LocalDate.now().plusDays(4),flightInfo3);
        FlightRide flightRide6 = new FlightRide(105,4,"BLR","HYD","30",LocalDate.now(),flightInfo3);

        flightClass3.put("EC",50);
        flightClass3.put("BC",10);
        flightClass3.put("FC",10);
        //LocalDate.parse("2019-08-21")
        FlightInfo flightInfo4 = new FlightInfo(3,"Boeing 787","Indigo",flightClass2);
        FlightRide flightRide7 = new FlightRide(106,3,"HYD","BLR","30",LocalDate.now().plusDays(1),flightInfo4);
        FlightRide flightRide8 = new FlightRide(107,4,"BLR","HYD","30",LocalDate.now(),flightInfo4);

        listOfFlights.add(flightRide1);
        listOfFlights.add(flightRide2);
        listOfFlights.add(flightRide3);
        listOfFlights.add(flightRide4);
        listOfFlights.add(flightRide5);
        listOfFlights.add(flightRide6);
        listOfFlights.add(flightRide7);
        listOfFlights.add(flightRide8);


        return listOfFlights;
  }

  public ArrayList<RideFare> initRideFareData() {

      ArrayList listOfRideFares = new ArrayList();
      //Initialising fare data
      RideFare rideFare1 = new RideFare(100, 1000, "EC", 8,5000);
      RideFare rideFare2 = new RideFare(100, 1001, "BC", 15,6000);
      RideFare rideFare3 = new RideFare(100, 1002, "FC", 12,8000);

      listOfRideFares.add(rideFare1);
      listOfRideFares.add(rideFare2);
      listOfRideFares.add(rideFare3);

      RideFare rideFare11 = new RideFare(102, 1003, "EC", 3,5000);
      RideFare rideFare22 = new RideFare(102, 1004, "BC", 10,6000);
      RideFare rideFare33 = new RideFare(102, 1005, "FC", 8,8000);

      listOfRideFares.add(rideFare11);
      listOfRideFares.add(rideFare22);
      listOfRideFares.add(rideFare33);

      RideFare rideFare111 = new RideFare(104, 1006, "EC", 30,3000);
      RideFare rideFare222 = new RideFare(104, 1007, "BC", 15,4000);
      RideFare rideFare333 = new RideFare(104, 1008, "FC", 5,5000);

      listOfRideFares.add(rideFare111);
      listOfRideFares.add(rideFare222);
      listOfRideFares.add(rideFare333);

      RideFare rideFare1111 = new RideFare(106, 1009, "EC", 30,3000);
      RideFare rideFare2222 = new RideFare(106, 1010, "BC", 15,4000);
      RideFare rideFare3333 = new RideFare(106, 1011, "FC", 5,5000);

      listOfRideFares.add(rideFare1111);
      listOfRideFares.add(rideFare2222);
      listOfRideFares.add(rideFare3333);



      return listOfRideFares;

  }
}
