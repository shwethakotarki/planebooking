import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.planebooking.Repository.FlightRepository;
import org.junit.Test;
import org.mockito.Mock;
import org.planebooking.Services.FlightSearchSvc;
import org.planebooking.model.FlightInfo;
import org.planebooking.model.FlightRide;
import org.planebooking.model.RideFare;
import org.planebooking.model.SearchCriteria;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestFlightSearch {

    @Mock
    FlightRepository flightRep;

    @InjectMocks
    FlightSearchSvc flightSearchService;

    @Test
    public void shouldReturnFlightsWhenSourceAndDestGiven()
    {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("HYD");
        searchCriteria.setDest("BLR");


        HashMap<FlightRide,RideFare> expectedflightRides = new HashMap<FlightRide,RideFare>();
        ArrayList<FlightRide> allFlights = new ArrayList<FlightRide>();
        allFlights = initData();

        ArrayList<RideFare> allRides = new ArrayList<RideFare>();
        allRides = initRideFareData();

        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
        flightClass1.put("EC",50);
        flightClass1.put("BC",20);
        flightClass1.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass1);
        FlightRide expectedFR1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
        RideFare rideFare1 = new RideFare(100, 1000, "EC", 8,5000);

        expectedflightRides.put(expectedFR1,rideFare1);

        when(flightRep.initData()).thenReturn(allFlights);
        when(flightRep.initRideFareData()).thenReturn(allRides);

        HashMap<FlightRide, RideFare> actualFR = flightSearchService.searchFlights(searchCriteria);
        Assert.assertEquals(expectedflightRides.size(),actualFR.size());

    }

    @Test
    public void shouldReturnFlightsWhenSourceDestandPassengersGiven()
    {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("HYD");
        searchCriteria.setDest("BLR");
        searchCriteria.setNoOfPass(2);


        HashMap<FlightRide,RideFare> expectedflightRides = new HashMap<FlightRide,RideFare>();
        ArrayList<FlightRide> allFlights = new ArrayList<FlightRide>();
        allFlights = initData();

        ArrayList<RideFare> allRides = new ArrayList<RideFare>();
        allRides = initRideFareData();

        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
        flightClass1.put("EC",50);
        flightClass1.put("BC",20);
        flightClass1.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass1);
        FlightRide expectedFR1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
        RideFare rideFare1 = new RideFare(100, 1000, "EC", 8,5000);

        expectedflightRides.put(expectedFR1,rideFare1);

        when(flightRep.initData()).thenReturn(allFlights);
        when(flightRep.initRideFareData()).thenReturn(allRides);

        HashMap<FlightRide, RideFare> actualFR = flightSearchService.searchFlights(searchCriteria);
        Assert.assertEquals(expectedflightRides.size(),actualFR.size());

    }

    @Test
    public void shouldReturnFlightsWhenSourceDestAndDateOfTravelGiven()
    {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("HYD");
        searchCriteria.setDest("BLR");
        searchCriteria.setNoOfPass(2);
        searchCriteria.setdateOfTravel(LocalDate.now().plusDays(1));

        HashMap<FlightRide,RideFare> expectedflightRides = new HashMap<FlightRide,RideFare>();
        ArrayList<FlightRide> allFlights = new ArrayList<FlightRide>();
        allFlights = initData();

        ArrayList<RideFare> allRides = new ArrayList<RideFare>();
        allRides = initRideFareData();

        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
        flightClass1.put("EC",50);
        flightClass1.put("BC",20);
        flightClass1.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass1);
        FlightRide expectedFR1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
        RideFare rideFare1 = new RideFare(100, 1000, "EC", 8,5000);

        expectedflightRides.put(expectedFR1,rideFare1);

        when(flightRep.initData()).thenReturn(allFlights);
        when(flightRep.initRideFareData()).thenReturn(allRides);

        HashMap<FlightRide, RideFare> actualFR = flightSearchService.searchFlights(searchCriteria);
        Assert.assertEquals(expectedflightRides.size(),actualFR.size());

    }

    @Test
    public void shouldReturnFlightsWhenSourceDestDOTAndClassGiven()
    {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("HYD");
        searchCriteria.setDest("BLR");
        searchCriteria.setNoOfPass(2);
        searchCriteria.setdateOfTravel(LocalDate.now().plusDays(1));
        searchCriteria.setClassType("EC");

        HashMap<FlightRide,RideFare> expectedflightRides = new HashMap<FlightRide,RideFare>();
        ArrayList<FlightRide> allFlights = new ArrayList<FlightRide>();
        allFlights = initData();

        ArrayList<RideFare> allRides = new ArrayList<RideFare>();
        allRides = initRideFareData();

        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
        flightClass1.put("EC",50);
        flightClass1.put("BC",20);
        flightClass1.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass1);
        FlightRide expectedFR1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
        RideFare rideFare1 = new RideFare(100, 1000, "EC", 8,5000);

        expectedflightRides.put(expectedFR1,rideFare1);

        when(flightRep.initData()).thenReturn(allFlights);
        when(flightRep.initRideFareData()).thenReturn(allRides);

        HashMap<FlightRide, RideFare> actualFR = flightSearchService.searchFlights(searchCriteria);
        Assert.assertEquals(expectedflightRides.size(),actualFR.size());

    }

    @Test
    public void shouldcalculateECFareFlightsWhenSourceDestDOTClassGiven()
    {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("HYD");
        searchCriteria.setDest("BLR");
        searchCriteria.setNoOfPass(2);
        searchCriteria.setdateOfTravel(LocalDate.now().plusDays(1));
        searchCriteria.setClassType("EC");
        double expectedTotalFare = 13000;
        Integer actualTotalcost=0;

        HashMap<FlightRide,RideFare> expectedflightRides = new HashMap<FlightRide,RideFare>();
        ArrayList<FlightRide> allFlights = new ArrayList<FlightRide>();
        allFlights = initData();

        ArrayList<RideFare> allRides = new ArrayList<RideFare>();
        allRides = initRideFareData();

        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
        flightClass1.put("EC",50); //20,25,5 {43 >= 21 && 43 <=45},6500 is the fare for one passenger
        flightClass1.put("BC",20);
        flightClass1.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass1);
        FlightRide expectedFR1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
        RideFare rideFare1 = new RideFare(100, 1000, "EC", 8,5000);

        expectedflightRides.put(expectedFR1,rideFare1);

        when(flightRep.initData()).thenReturn(allFlights);
        when(flightRep.initRideFareData()).thenReturn(allRides);

        HashMap<FlightRide, RideFare> actualFR = flightSearchService.searchFlights(searchCriteria);
        for (Map.Entry<FlightRide,RideFare> entry : actualFR.entrySet())
            actualTotalcost= (int)entry.getValue().getTotalCost();
        Assert.assertEquals((long)expectedTotalFare,(long)actualTotalcost);

    }

    @Test
    public void shouldcalculateBCFareFlightsWhenSourceDestDOTClassGiven()
    {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("HYD");
        searchCriteria.setDest("BLR");
        searchCriteria.setNoOfPass(2);
        searchCriteria.setdateOfTravel(LocalDate.now().plusDays(1));
        searchCriteria.setClassType("BC");
        double expectedTotalFare = 12000;
        Integer actualTotalcost=0;

        HashMap<FlightRide,RideFare> expectedflightRides = new HashMap<FlightRide,RideFare>();
        ArrayList<FlightRide> allFlights = new ArrayList<FlightRide>();
        allFlights = initData();

        ArrayList<RideFare> allRides = new ArrayList<RideFare>();
        allRides = initRideFareData();

        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
        flightClass1.put("EC",50);
        flightClass1.put("BC",20);
        flightClass1.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass1);
        FlightRide expectedFR1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
        RideFare rideFare1 = new RideFare(100, 1001, "BC", 15,6000);

        expectedflightRides.put(expectedFR1,rideFare1);

        when(flightRep.initData()).thenReturn(allFlights);
        when(flightRep.initRideFareData()).thenReturn(allRides);

        HashMap<FlightRide, RideFare> actualFR = flightSearchService.searchFlights(searchCriteria);
        for (Map.Entry<FlightRide,RideFare> entry : actualFR.entrySet())
            actualTotalcost= (int)entry.getValue().getTotalCost();

        Assert.assertEquals((long)expectedTotalFare,(long)actualTotalcost);

    }

    @Test
    public void shouldcalculateFCFareFlightsWhenSourceDestDOTClassGiven()
    {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("HYD");
        searchCriteria.setDest("BLR");
        searchCriteria.setNoOfPass(2);
        searchCriteria.setdateOfTravel(LocalDate.now().plusDays(1));
        searchCriteria.setClassType("FC");
        double expectedTotalFare = 32000; //one day before--100 % increase in fare
        Integer actualTotalcost=0;

        HashMap<FlightRide,RideFare> expectedflightRides = new HashMap<FlightRide,RideFare>();
        ArrayList<FlightRide> allFlights = new ArrayList<FlightRide>();
        allFlights = initData();

        ArrayList<RideFare> allRides = new ArrayList<RideFare>();
        allRides = initRideFareData();

        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();
        flightClass1.put("EC",50);
        flightClass1.put("BC",20);
        flightClass1.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass1);
        FlightRide expectedFR1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
        RideFare rideFare1 = new RideFare(100, 1002, "FC", 12,8000);

        expectedflightRides.put(expectedFR1,rideFare1);

        when(flightRep.initData()).thenReturn(allFlights);
        when(flightRep.initRideFareData()).thenReturn(allRides);

        HashMap<FlightRide, RideFare> actualFR = flightSearchService.searchFlights(searchCriteria);
        for (Map.Entry<FlightRide,RideFare> entry : actualFR.entrySet())
            actualTotalcost= (int)entry.getValue().getTotalCost();

        Assert.assertEquals((long)expectedTotalFare,(long)actualTotalcost);

    }

    public ArrayList<FlightRide> initData()
    {
        HashMap<String,Integer> flightClass = new HashMap<String,Integer>();
        ArrayList listOfFlights = new ArrayList();
        HashMap<String,Integer> flightClass1 = new HashMap<String,Integer>();

        FlightInfo flightInfo;
        FlightRide flightRide;

        flightClass.put("EC",50);
        flightClass.put("BC",20);
        flightClass.put("FC",30);

        FlightInfo flightInfo1 = new FlightInfo(1,"Boeing 777- 200LR(77L)","AIR INDIA",flightClass);
        FlightRide flightRide1 = new FlightRide(100,1,"HYD","BLR","20",LocalDate.now().plusDays(1),flightInfo1);
       // FlightRide flightRide2 = new FlightRide(101,1,"BLR","HYD","20",LocalDate.now(),flightInfo1);


        flightClass1.put("EC",50);
        flightClass1.put("BC",0);
        flightClass1.put("FC",10);

        FlightInfo flightInfo2 = new FlightInfo(2,"Airbus A319 V2","SPICE JET",flightClass1);
        FlightRide flightRide3 = new FlightRide(102,2,"HYD","BLR","30",LocalDate.now().plusDays(5),flightInfo2);
        //FlightRide flightRide4 = new FlightRide(103,2,"BLR","HYD","30",LocalDate.now().plusDays(5),flightInfo2);

        listOfFlights.add(flightRide1);
      //  listOfFlights.add(flightRide2);
        listOfFlights.add(flightRide3);
        //listOfFlights.add(flightRide4);

        return listOfFlights;
    }

    public ArrayList<RideFare> initRideFareData()
    {
        ArrayList listOfRideFares = new ArrayList();
        //Initialising fare data
        RideFare rideFare1 = new RideFare(100, 1000, "EC", 8,5000);
        RideFare rideFare2 = new RideFare(100, 1001, "BC", 15,6000);
        RideFare rideFare3 = new RideFare(100, 1002, "FC", 12,8000);


        listOfRideFares.add(rideFare1);
        listOfRideFares.add(rideFare2);
        listOfRideFares.add(rideFare3);

        RideFare rideFare11 = new RideFare(102, 1003, "EC", 3,5000);
        RideFare rideFare22 = new RideFare(102, 1004, "BC", 10,6000);
        RideFare rideFare33 = new RideFare(102, 1005, "FC", 8,8000);

        listOfRideFares.add(rideFare11);
        listOfRideFares.add(rideFare22);
        listOfRideFares.add(rideFare33);


        return listOfRideFares;

    }
}
